import { callApi } from '../helpers/apiHelper';
import { fighter } from '../models/fighter';
import { fighterDetails } from '../models/fighterDetails';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET') as fighter[];
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id : string) {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET') as fighterDetails;
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

