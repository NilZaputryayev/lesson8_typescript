import { createElement } from './helpers/domHelper';
export function createFighter(fighter, handleClick, selectFighter) {
    const { name, source } = fighter;
    const nameElement = createName(name);
    const imageElement = createImage(source);
    const checkboxElement = createCheckbox();
    const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
    fighterContainer.append(imageElement, nameElement, checkboxElement);
    const preventCheckboxClick = (ev) => ev.stopPropagation();
    const onCheckboxClick = (ev) => selectFighter(ev, fighter);
    const onFighterClick = (ev) => handleClick(ev, fighter);
    fighterContainer.addEventListener('click', onFighterClick, false);
    checkboxElement.addEventListener('change', onCheckboxClick, false);
    checkboxElement.addEventListener('click', preventCheckboxClick, false);
    return fighterContainer;
}
function createName(name) {
    const nameElement = createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;
    return nameElement;
}
function createImage(source) {
    const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: [{ key: 'src', value: source }] });
    return imgElement;
}
function createCheckbox() {
    const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
    const span = createElement({ tagName: 'span', className: 'checkmark' });
    const attr = { key: 'type', value: 'checkbox' };
    const checkboxElement = createElement({ tagName: 'input', attributes: [] = [attr] });
    label.append(checkboxElement, span);
    return label;
}
//# sourceMappingURL=fighterView.js.map