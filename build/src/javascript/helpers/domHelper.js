export function createElement(elementToCreate) {
    const element = document.createElement(elementToCreate.tagName);
    if (elementToCreate.className) {
        element.classList.add(elementToCreate.className);
    }
    if (elementToCreate.attributes) {
        elementToCreate.attributes.forEach(attribute => element.setAttribute(attribute.key, attribute.value));
    }
    return element;
}
//# sourceMappingURL=domHelper.js.map