import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { fighterDetails } from './models/fighterDetails';
import { fighter } from './models/fighter';
import { fightersDetails } from './helpers/mockData';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters : fighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<string,fighterDetails>();

async function showFighterDetails(event : Event, fighter : fighterDetails) {
  const fullInfo = await getFighterInfo(fighter._id);
  if(fullInfo)
    showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) {
  const info = fightersDetailsCache.has(fighterId) ? fightersDetailsCache.get(fighterId) :
     await getFighterDetails(fighterId); 
       
  return info;
}

function createFightersSelector() {

  const selectedFighters = new Map<string,fighterDetails>();

  return async function selectFighterForBattle(event : Event, fighter : fighter) {

    const fullInfo = await getFighterInfo(fighter._id);   

    if(fullInfo)
    {
      if((<HTMLInputElement>(event.target)).checked) {
        selectedFighters.set(fighter._id, fullInfo);    

      } else { 
        selectedFighters.delete(fighter._id);
        
      }

      if (selectedFighters.size === 2) {
  
        const winner = fight(selectedFighters.values().next().value, selectedFighters.get(fighter._id)!);
        showWinnerModal(winner!);
      }
    }
  }
}
