import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { fighterDetails } from '../models/fighterDetails'
import { attribute } from '../models/attribute';

export  function showFighterDetailsModal(fighter : fighterDetails) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal( title, bodyElement );
}

function createFighterDetails(fighter : fighterDetails) {
  const { name,attack,defense,health,source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  const infoElement = createElement({ tagName: 'div', className: 'fighter-wrapper' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-info' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-info' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-info' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes :[<attribute>{ key: 'src', value:source }] });

  // show fighter name, attack, defense, health, image


  nameElement.innerText = name;
  attackElement.innerText = `Attack: ${  attack.toLocaleString()}`;
  defenseElement.innerText = `Defence: ${ defense.toLocaleString()}`;
  healthElement.innerText = `Health: ${ health.toLocaleString()}`;

  infoElement.appendChild(attackElement);
  infoElement.appendChild(defenseElement);
  infoElement.appendChild(healthElement);

  fighterDetails.append(nameElement);
  fighterDetails.append(imageElement);
  fighterDetails.append(infoElement);
  


  return fighterDetails;
}
