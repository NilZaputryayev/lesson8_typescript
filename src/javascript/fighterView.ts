import { createElement } from './helpers/domHelper'; 
import { fighter } from './models/fighter'; 
import { domElement } from './models/domElement';
import { attribute } from './models/attribute';


export function createFighter(fighter : fighter, handleClick : Function, selectFighter:Function) {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev:Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name : string) {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source : string) {
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes :[<attribute>{ key: 'src', value:source }] });

  return imgElement;
}

function createCheckbox() {
  
  const label = createElement({ tagName: 'label', className: 'custom-checkbox' } as domElement);
  const span = createElement({ tagName: 'span', className: 'checkmark' } as domElement);

  const attr = (<attribute>{ key: 'type', value:'checkbox' });
 
  const checkboxElement = createElement({ tagName: 'input', attributes : [] = [attr] });

  label.append(checkboxElement, span);
  return label;
}