import { attribute } from "./attribute";

export interface domElement {
    tagName : string;
    className? : string;
    attributes? : attribute[]
}