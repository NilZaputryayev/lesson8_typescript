import { domElement } from "../models/domElement";

export function createElement( elementToCreate : domElement) {
 
  const element = document.createElement(elementToCreate.tagName);
  
  if (elementToCreate.className) {
    element.classList.add(elementToCreate.className);
  }
 
if(elementToCreate.attributes) {
 elementToCreate.attributes.forEach(attribute => element.setAttribute(attribute.key, attribute.value));
}
  return element;
}