export function fight(firstFighter, secondFighter) {
    let health1 = firstFighter.health;
    let health2 = secondFighter.health;
    let damage = 0;
    let i = 1;
    do {
        damage = getDamage(firstFighter, secondFighter);
        if (damage > health2) {
            console.log(secondFighter.name, health2, firstFighter.name, health1);
            return firstFighter;
        }
        health2 = health2 - damage;
        damage = getDamage(secondFighter, firstFighter);
        if (damage > health1) {
            console.log(secondFighter.name, health2, firstFighter.name, health1);
            return secondFighter;
        }
        health1 = health1 - damage;
        i++;
    } while (i < 100);
}
export function getDamage(attacker, enemy) {
    const hit = getHitPower(attacker);
    const block = getBlockPower(enemy);
    const damage = hit > block ? hit - block : 0;
    return damage;
}
export function getHitPower(fighter) {
    const attack = fighter.attack * (1 + Math.random());
    return attack;
}
export function getBlockPower(fighter) {
    const def = fighter.defense * (1 + Math.random());
    return def;
}
//# sourceMappingURL=fight.js.map