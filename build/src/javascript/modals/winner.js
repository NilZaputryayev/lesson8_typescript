import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showWinnerModal(fighter) {
    // show winner name and image
    ;
    const title = `And the Winner is ... ${fighter.name} !`;
    const body = createElement({ tagName: 'div', className: 'modal-body' });
    const content = createElement({ tagName: 'img', className: 'fighter-image', attributes: [{ key: 'src', value: fighter.source }] });
    body.append(content);
    showModal(title, body);
}
//# sourceMappingURL=winner.js.map